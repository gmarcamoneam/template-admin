import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { PlaceService } from '../place.service';

@Component({
  selector: 'app-addplace',
  templateUrl: './addplace.component.html',
  styleUrls: ['./addplace.component.css'],
})
export class AddplaceComponent implements OnInit {
  place: FormGroup;
  constructor(
    private formbuilder: FormBuilder,
    private placeservice: PlaceService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.place = this.formbuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
  }
  addplace() {
    this.placeservice.createplace(this.place.value).subscribe((res: any) => {
      console.log(res['data']);
      Swal.fire('place added');
    });
    this.route.navigateByUrl('/home/listplace');
  }
}
