import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from './component/sidebar/category/category.component';
import { HomeComponent } from './component/home/home.component';
import { LayoutComponent } from './component/layout/layout.component';
import { DetailoffreComponent } from './detailoffre/detailoffre.component';
import { ListcontratComponent } from './listcontrat/listcontrat.component';
import { ListentrepriseComponent } from './listentreprise/listentreprise.component';
import { ListoffreComponent } from './listoffre/listoffre.component';
import { ListplaceComponent } from './listplace/listplace.component';
import { ListskillComponent } from './listskill/listskill.component';
import { UpdateoffreComponent } from './updateoffre/updateoffre.component';
import { AddcategoryComponent } from './component/sidebar/addcategory/addcategory.component';
import { RegistreadminComponent } from './registreadmin/registreadmin.component';
import { LoginComponent } from './login/login.component';
import { AddplaceComponent } from './addplace/addplace.component';
import { ModalComponent } from './components/modal/modal.component';
import { ProfileComponent } from './profile/profile.component';
import { CondidatureComponent } from './condidature/condidature.component';

const routes: Routes = [
  { path: 'registeradmin', component: RegistreadminComponent },
  { path: '', component: LoginComponent },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: '', component: LayoutComponent },
      { path: 'listoffre', component: ListoffreComponent },
      { path: 'listplace', component: ListplaceComponent },
      { path: 'listentreprise', component: ListentrepriseComponent },
      { path: 'listcontrat', component: ListcontratComponent },
      { path: 'listskill', component: ListskillComponent },
      { path: 'updateoffre/:id', component: UpdateoffreComponent },
      { path: 'detailoffre/:id', component: DetailoffreComponent },
      { path: 'category', component: CategoryComponent },
      { path: 'addcategory', component: AddcategoryComponent },
      { path: 'addplace', component: AddplaceComponent },
      { path: 'modal', component: ModalComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'condidature', component: CondidatureComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
