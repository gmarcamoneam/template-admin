import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { FooterComponent } from './component/footer/footer.component';
import { LayoutComponent } from './component/layout/layout.component';
import { HomeComponent } from './component/home/home.component';
import { ListoffreComponent } from './listoffre/listoffre.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RecherchePipe } from './pipes/recherche.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListplaceComponent } from './listplace/listplace.component';
import { ListcontratComponent } from './listcontrat/listcontrat.component';
import { ListentrepriseComponent } from './listentreprise/listentreprise.component';
import { AddplaceComponent } from './addplace/addplace.component';
import { ListskillComponent } from './listskill/listskill.component';
import { UpdateoffreComponent } from './updateoffre/updateoffre.component';
import { DetailoffreComponent } from './detailoffre/detailoffre.component';
import { CategoryComponent } from './component/sidebar/category/category.component';
import { AddcategoryComponent } from './component/sidebar/addcategory/addcategory.component';
import { RegistreadminComponent } from './registreadmin/registreadmin.component';
import { LoginComponent } from './login/login.component';
import { ModalComponent } from './components/modal/modal.component';
import { ProfileComponent } from './profile/profile.component';
import { CondidatureComponent } from './condidature/condidature.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    LayoutComponent,
    HomeComponent,
    ListoffreComponent,
    RecherchePipe,
    ListplaceComponent,
    ListcontratComponent,
    ListentrepriseComponent,
    AddplaceComponent,
    ListskillComponent,
    UpdateoffreComponent,
    DetailoffreComponent,
    CategoryComponent,
    AddcategoryComponent,
    RegistreadminComponent,
    LoginComponent,
    ModalComponent,
    ProfileComponent,
    CondidatureComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule, //update
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
