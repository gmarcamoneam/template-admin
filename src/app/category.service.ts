import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallcategory() {
    return this.http.get(`${environment.baseurl}/getAllCategories`);
  }
  addcategory(category: any) {
    return this.http.post(`${environment.baseurl}/createcategory`, category, {
      headers: this.headersoption,
    });
  }

  deletecategory(id: any) {
    return this.http.delete(`${environment.baseurl}/deleteCategory/${id}`, {
      headers: this.headersoption,
    });
  }
  updatecategory(id: any, category: any) {
    return this.http.put(
      `${environment.baseurl}/updateCategory/${id}`,
      category,
      { headers: this.headersoption }
    );
  }
}
