import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/category.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
})
export class CategoryComponent implements OnInit {
  listcategory: any;
  form: FormGroup;
  p: number = 1;
  categoryupdate: FormGroup;
  constructor(
    private category: CategoryService,
    private frombuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.gelallcategories();
    this.categoryupdate = this.frombuilder.group({
      _id: ['', Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
  }
  gelallcategories() {
    this.category.getallcategory().subscribe((res: any) => {
      this.listcategory = res['data'];
      console.log('liste of category', this.listcategory);
    });
  }

  deletecategory(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.category.deletecategory(id).subscribe((res: any) => {
          console.log(res);
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.gelallcategories();
        });
      }
    });
  }

  open(p: any) {
    this.categoryupdate.patchValue({
      name: p.name,
      description: p.description,
      _id: p._id,
    });
  }
  updatecategorys() {
    this.category
      .updatecategory(this.categoryupdate.value._id, this.categoryupdate.value)
      .subscribe((res: any) => {
        console.log(res);
        this.gelallcategories();
      });
  }
}
