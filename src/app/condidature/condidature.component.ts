import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';

@Component({
  selector: 'app-condidature',
  templateUrl: './condidature.component.html',
  styleUrls: ['./condidature.component.css'],
})
export class CondidatureComponent implements OnInit {
  listcondidature: any;
  listcondidate: any;
  constructor(private offreService: OffreService) {}

  ngOnInit(): void {
    this.getallcondidatures();
  }
  getallcondidatures() {
    this.offreService.getallcondidature().subscribe((res: any) => {
      this.listcondidature = res['data'];
      console.log('liste des condidatures', this.listcondidature);
    });
  }
  deletecondidatures(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.offreService.deletecondidature(id).subscribe((res: any) => {
          console.log(res);
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
        });
      }
    });
  }
}
