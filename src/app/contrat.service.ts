import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ContratService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallcontrat() {
    return this.http.get(`${environment.baseurl}/getAllContrats`);
  }
  deletecontrat(id: any) {
    return this.http.delete(`${environment.baseurl}/deletecontrat/${id}`, {
      headers: this.headersoption,
    });
  }
  createcontrat(contat: any) {
    return this.http.post(`${environment.baseurl}/createcontrat`, contat, {
      headers: this.headersoption,
    });
  }
  updatecontrat(id: any, contat: any) {
    return this.http.put(`${environment.baseurl}/updatecontrat/${id}`, contat, {
      headers: this.headersoption,
    });
  }
}
