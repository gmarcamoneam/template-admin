import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OffreService } from '../offre.service';

@Component({
  selector: 'app-detailoffre',
  templateUrl: './detailoffre.component.html',
  styleUrls: ['./detailoffre.component.css'],
})
export class DetailoffreComponent implements OnInit {
  id = this.activeroute.snapshot.params['id'];
  offre: any;
  constructor(
    private activeroute: ActivatedRoute,
    private offreservice: OffreService
  ) {}

  ngOnInit(): void {
    console.log('id', this.id);
    this.getoffrebyid();
  }
  getoffrebyid() {
    this.offreservice.getoffrebyid(this.id).subscribe((res: any) => {
      this.offre = res['data'];
      console.log('détail offre', this.offre);
    });
  }
}
