import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EntrepriseService {
  constructor(private http: HttpClient) {}
  getentreprise() {
    return this.http.get(`${environment.baseurl}/getalluser`);
  }
  deleteentreprise(id: any) {
    return this.http.delete(`${environment.baseurl}/deleteentreprise/${id}`);
  }
  confirmentreprise(id: any) {
    return this.http.get(`${environment.baseurl}/confirmUser/${id}`);
  }
}
