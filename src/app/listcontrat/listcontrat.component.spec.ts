import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListcontratComponent } from './listcontrat.component';

describe('ListcontratComponent', () => {
  let component: ListcontratComponent;
  let fixture: ComponentFixture<ListcontratComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListcontratComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListcontratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
