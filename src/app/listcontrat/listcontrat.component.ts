import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ContratService } from '../contrat.service';

@Component({
  selector: 'app-listcontrat',
  templateUrl: './listcontrat.component.html',
  styleUrls: ['./listcontrat.component.css'],
})
export class ListcontratComponent implements OnInit {
  listcontrat: any;
  p: number = 1;
  term: any;
  formajout: FormGroup;
  formupdate: FormGroup;
  constructor(
    private contratservice: ContratService,
    private formbuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getallcontrat();
    //added
    this.formajout = this.formbuilder.group({
      type: ['', Validators.required],
    });
    //update
    this.formupdate = this.formbuilder.group({
      _id: ['', Validators.required],
      type: ['', Validators.required],
    });
  }
  getallcontrat() {
    this.contratservice.getallcontrat().subscribe((res: any) => {
      this.listcontrat = res['data'];
      console.log('list of contrats', this.listcontrat);
    });
  }
  deletecontrats(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.contratservice.deletecontrat(id).subscribe((res: any) => {
          console.log(res);
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getallcontrat();
        });
      }
    });
  }
  addedcontrat() {
    this.contratservice
      .createcontrat(this.formajout.value)
      .subscribe((res: any) => {
        console.log(res['data']);
        Swal.fire('type of contrat added');
      });
  }
  open(p: any) {
    this.formupdate.patchValue({
      _id: p._id,
      type: p.type,
    });
  }
  updatecontrats() {
    this.contratservice
      .updatecontrat(this.formupdate.value._id, this.formupdate.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('type updated');
      });
  }
}
