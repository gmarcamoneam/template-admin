import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { EntrepriseService } from '../entreprise.service';

@Component({
  selector: 'app-listentreprise',
  templateUrl: './listentreprise.component.html',
  styleUrls: ['./listentreprise.component.css'],
})
export class ListentrepriseComponent implements OnInit {
  listentreprise: any;
  p: number = 2;
  confirmer: any;
  constructor(private x: EntrepriseService) {}

  ngOnInit(): void {
    this.getenterprise();
  }
  getenterprise() {
    this.x.getentreprise().subscribe((res: any) => {
      this.listentreprise = res['data'].filter(
        (a: any) => a.role == 'entreprise'
      );
      console.log('listes des entreprise', this.listentreprise);
    });
  }
  deleteentreprises(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.x.deleteentreprise(id).subscribe((res: any) => {
          console.log(res);
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getenterprise();
        });
      }
    });
  }

  confirmentreprises(id: any) {
    this.x.confirmentreprise(id).subscribe(() => {
      this.confirmer;
    });
  }
}
