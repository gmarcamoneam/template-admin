import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';

@Component({
  selector: 'app-listoffre',
  templateUrl: './listoffre.component.html',
  styleUrls: ['./listoffre.component.css'],
})
export class ListoffreComponent implements OnInit {
  listoffre: any;
  confirm: any;
  length: any;
  p: number = 1;
  term: any;
  page: any;
  limit: any;
  date = new Date().toISOString().split('T')[0].toString();

  constructor(private offreService: OffreService) {}

  ngOnInit(): void {
    this.getalloffre();
  }
  getalloffre() {
    this.offreService.getoffre(this.page, this.limit).subscribe((res: any) => {
      this.listoffre = res['data'].filter((element: any) => {
        return Date.parse(element.dateexpiration) >= Date.parse(this.date);
      });
      console.log('liste des offres', this.listoffre);
    });
  }
  deleteoffres(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.offreService.deleteoffre(id).subscribe((res: any) => {
          console.log(res);
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getalloffre();
        });
      }
    });
  }
  confirmoffres(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, confirm it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.offreService.confirmoffre(id).subscribe((res: any) => {
          console.log(res['data']);
          Swal.fire('confirmed!', 'Your file has been confirmed.', 'success');
          this.getalloffre();
        });
      }
    });
  }
}
