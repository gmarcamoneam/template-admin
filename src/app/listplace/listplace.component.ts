import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { PlaceService } from '../place.service';

@Component({
  selector: 'app-listplace',
  templateUrl: './listplace.component.html',
  styleUrls: ['./listplace.component.css'],
})
export class ListplaceComponent implements OnInit {
  listplace: any;
  p: number = 1;
  term: any;
  place: FormGroup;
  placeajout: FormGroup;
  constructor(
    private placeservice: PlaceService,
    private formbilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.place = this.formbilder.group({
      _id: ['', Validators.required],
      name: ['', Validators.required],
    });
    this.placeajout = this.formbilder.group({
      name: ['', Validators.required],
    });
    this.getallplaces();
  }

  getallplaces() {
    this.placeservice.getallplace().subscribe((res: any) => {
      this.listplace = res['data'];
      console.log('list of places', this.listplace);
    });
  }
  deleteplaces(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.placeservice.deleteplace(id).subscribe((res: any) => {
          console.log(res);
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getallplaces();
        });
      }
    });
  }
  open(p: any) {
    this.place.patchValue({
      name: p.name,
      _id: p._id,
    });
  }
  updateplace() {
    this.placeservice
      .updateplace(this.place.value._id, this.place.value)
      .subscribe((res: any) => {
        console.log(res);
        this.getallplaces();
      });
  }
  addplace() {
    this.placeservice
      .createplace(this.placeajout.value)
      .subscribe((res: any) => {
        console.log(res['data']);
        this.getallplaces();
      });
  }
}
