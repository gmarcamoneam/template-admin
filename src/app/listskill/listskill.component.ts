import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { SkillService } from '../skill.service';

@Component({
  selector: 'app-listskill',
  templateUrl: './listskill.component.html',
  styleUrls: ['./listskill.component.css'],
})
export class ListskillComponent implements OnInit {
  listskill: any;
  term: any;
  p: number = 1;
  formadd: FormGroup;
  formupdate: FormGroup;
  constructor(private skill: SkillService, private formbilder: FormBuilder) {}

  ngOnInit(): void {
    this.getskills();
    //added
    this.formadd = this.formbilder.group({
      nbexperience: ['', Validators.required],
      description: ['', Validators.required],
    });
    //update
    this.formupdate = this.formbilder.group({
      _id: ['', Validators.required],
      nbexperience: ['', Validators.required],
      description: ['', Validators.required],
    });
  }
  getskills() {
    this.skill.getallskill().subscribe((res: any) => {
      this.listskill = res['data'];
      console.log('list of skills', this.listskill);
    });
  }
  deleteckills(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.skill.deleteskill(id).subscribe((res: any) => {
          console.log(res);
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getskills();
        });
      }
    });
  }
  createskills() {
    this.skill.createskils(this.formadd.value).subscribe((res: any) => {
      console.log(res['data']);

      Swal.fire('skills Added');
    });
  }
  open(p: any) {
    this.formupdate.patchValue({
      nbexperience: p.nbexperience,
      description: p.description,
      _id: p._id,
    });
  }

  updateskills() {
    //formupdate:FormGroup
    this.skill
      .updateskill(this.formupdate.value._id, this.formupdate.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('skill updated');
        this.getskills();
      });
  }
}
