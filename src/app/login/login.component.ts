import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RegistreadminService } from '../registreadmin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  constructor(
    private form: FormBuilder,
    private register: RegistreadminService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.login = this.form.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  logins() {
    this.register.login(this.login.value).subscribe((res: any) => {
      console.log(res);
      Swal.fire('connected');
      // this.route.navigateByUrl('/home')
      //if(res.message==="hurray! you are nouw loged in")
      if (res.success === true) {
        localStorage.setItem('userconnect', JSON.stringify(res.user));
        localStorage.setItem('token', res.token);
        //localStorage.setItem('refrechtoken',res.refrechtoken)
        localStorage.setItem('state', '0');
        this.route.navigateByUrl('/home/listentreprise');
      }
    });
  }
}
