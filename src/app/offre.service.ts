import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OffreService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getoffre(page: any, limit: any) {
    return this.http.get(
      `${environment.baseurl}/getAlloffres?page=${page}&limit=${limit}`
    );
  }
  deleteoffre(id: any) {
    return this.http.delete(`${environment.baseurl}/deleteoffre/${id}`, {
      headers: this.headersoption,
    });
  }

  confirmoffre(id: any) {
    return this.http.get(`${environment.baseurl}/comfirmoffre/${id}`);
  }
  getoffrebyid(id: any) {
    return this.http.get(`${environment.baseurl}/getoffreById/${id}`);
  }
  updateoffre(id: any, offre: any) {
    return this.http.put(`${environment.baseurl}/updateoffre/${id}`, offre, {
      headers: this.headersoption,
    });
  }
  getallcondidature() {
    return this.http.get(`${environment.baseurl}/getAllcandidatures`);
  }
  deletecondidature(id: any) {
    return this.http.delete(`${environment.baseurl}/deletecandidature/${id}`, {
      headers: this.headersoption,
    });
  }
}
