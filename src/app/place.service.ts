import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PlaceService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallplace() {
    return this.http.get(`${environment.baseurl}/getAllplaces`);
  }

  deleteplace(id: any) {
    return this.http.delete(`${environment.baseurl}/deleteplace/${id}`, {
      headers: this.headersoption,
    });
  }
  createplace(place: any) {
    return this.http.post(`${environment.baseurl}/createplace`, place, {
      headers: this.headersoption,
    });
  }
  updateplace(id: any, place: any) {
    return this.http.put(`${environment.baseurl}/updateplace/${id}`, place, {
      headers: this.headersoption,
    });
  }
}
