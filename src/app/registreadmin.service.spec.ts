import { TestBed } from '@angular/core/testing';

import { RegistreadminService } from './registreadmin.service';

describe('RegistreadminService', () => {
  let service: RegistreadminService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegistreadminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
