import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RegistreadminService {
  constructor(private http: HttpClient) {}
  login(login: any) {
    return this.http.post(`${environment.baseurl}/login`, login);
  }
  registeradmin(admin: any) {
    return this.http.post(`${environment.baseurl}/registreAdmin`, admin);
  }
}
