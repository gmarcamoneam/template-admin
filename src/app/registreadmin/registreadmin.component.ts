import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistreadminService } from '../registreadmin.service';

@Component({
  selector: 'app-registreadmin',
  templateUrl: './registreadmin.component.html',
  styleUrls: ['./registreadmin.component.css'],
})
export class RegistreadminComponent implements OnInit {
  admin: FormGroup;
  constructor(
    private form: FormBuilder,
    private register: RegistreadminService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.admin = this.form.group({
      fullname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  registeradmin() {
    this.register.registeradmin(this.admin.value).subscribe((res: any) => {
      console.log(res);
      this.route.navigateByUrl('/');
    });
  }
}
