import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SkillService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallskill() {
    return this.http.get(`${environment.baseurl}/getAllSkills`);
  }
  deleteskill(id: any) {
    return this.http.delete(`${environment.baseurl}/deleteSkill/${id}`, {
      headers: this.headersoption,
    });
  }
  createskils(skils: any) {
    return this.http.post(`${environment.baseurl}/createskill`, skils, {
      headers: this.headersoption,
    });
  }
  updateskill(id: any, skill: any) {
    return this.http.put(`${environment.baseurl}/updateSkill/${id}`, skill, {
      headers: this.headersoption,
    });
  }
}
