import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { CategoryService } from '../category.service';
import { ContratService } from '../contrat.service';
import { EntrepriseService } from '../entreprise.service';
import { OffreService } from '../offre.service';
import { PlaceService } from '../place.service';
import { SkillService } from '../skill.service';
@Component({
  selector: 'app-updateoffre',
  templateUrl: './updateoffre.component.html',
  styleUrls: ['./updateoffre.component.css'],
})
export class UpdateoffreComponent implements OnInit {
  id = this.active.snapshot.params['id'];
  listcontrat: any;
  listplace: any;
  listskill: any;
  listcategory: any;
  listentreprise: any;
  offre: any;
  length: any;
  form: FormGroup;
  constructor(
    private formbuilder: FormBuilder,
    private contratservice: ContratService,
    private active: ActivatedRoute,
    private offreservice: OffreService,
    private placeservice: PlaceService,
    private skill: SkillService,
    private category: CategoryService,
    private entreprise: EntrepriseService, //entreprises

    private route: Router
  ) {}

  ngOnInit(): void {
    this.getallcontrat();
    this.getoffrebyid();
    this.getallplaces();
    this.getallskills();
    this.getallcategory();
    this.getallentreprise();
    this.form = this.formbuilder.group({
      titre: ['', Validators.required],
      description: ['', Validators.required],
      dateexpiration: ['', Validators.required],
      salaire: ['', Validators.required],
      contrat: ['', Validators.required],
      skill: ['', Validators.required],
      category: ['', Validators.required],
      place: ['', Validators.required],
    });
    console.log('id', this.id);
  }
  getoffrebyid() {
    this.offreservice.getoffrebyid(this.id).subscribe((res: any) => {
      this.active = res['data'];
      console.log('detail offre', res['data']);
      this.form.patchValue({
        titre: res['data'].titre,
        description: res['data'].description,
        salaire: res['data'].salaire,
        dateexpiration: res['data'].dateexpiration,

        place: res['data'].place._id,
        contrat: res['data'].contrat._id,
        skill: res['data'].skill._id,
        category: res['data'].category._id,
      });
    });
  }
  getallcontrat() {
    this.contratservice.getallcontrat().subscribe((res: any) => {
      this.listcontrat = res['data'];
      console.log('list of contrats', this.listcontrat);
    });
  }

  getallplaces() {
    this.placeservice.getallplace().subscribe((res: any) => {
      this.listplace = res['data'];
      console.log('list of places', this.listplace);
    });
  }
  getallskills() {
    this.skill.getallskill().subscribe((res: any) => {
      this.listskill = res['data'];
      console.log('list of skills', this.listskill);
    });
  }
  getallcategory() {
    this.category.getallcategory().subscribe((res: any) => {
      this.listcategory = res['data'];
      console.log('list of category', this.listcategory);
    });
  }
  getallentreprise() {
    this.entreprise.getentreprise().subscribe((res: any) => {
      this.listentreprise = res['data'];
      console.log('list of entreprise', this.listentreprise);
    });
  }
  updateoffres() {
    this.offreservice
      .updateoffre(this.id, this.form.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('offre updated');
        this.route.navigateByUrl('/home/listoffre');
      });
  }
}
